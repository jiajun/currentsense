# Firmware for Voltage/Current Meter

## IDE

- System Workbench for STM32
- STM32CubeMX

## Hardware

- STM32F042K6T6 (Mainstream ARM Cortex-M0 USB line MCU with 32 Kbytes Flash, 48 MHz CPU, USB, CAN and CEC functions)
- INA226 (36-V, Bi-Directional, Ultra-High Accuracy, Low-/High-Side, I2C Out Current/Power Monitor w/ Alert)

## Board

https://bitbucket.org/jiajun/hardware/src/master/MiniMultimeter/

![Board](board.png)


