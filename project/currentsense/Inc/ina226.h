/*
 * iha226.h
 *
 *  Created on: Aug 2, 2017
 *      Author: user
 */

#ifndef INA226_H_
#define INA226_H_

typedef struct {
   ushort raw;
   double value;
} ina226_data_t;

typedef struct {
   ushort raw;
   ushort corrected;
} ina226_cal_t;

typedef enum {
	AVERAGE_1=0,
	AVERAGE_4=1,
	AVERAGE_16=2,
	AVERAGE_64=3,
	AVERAGE_128=4,
	AVERAGE_256=5,
	AVERAGE_512=6,
	AVERAGE_1024=7
} INA226_AVERAGING_MODE;

typedef enum {
	VCT_US_140=0,
	VCT_US_204=1,
	VCT_US_332=2,
	VCT_US_588=3,
	VCT_US_1100=4,
	VCT_US_2116=5,
	VCT_US_4156=6,
	VCT_US_8244=7
} INA226_VCT;

typedef enum {
	MAX_I_20=0,
	MAX_I_16=1,
	MAX_I_13=2,
	MAX_I_10=3,
	MAX_I_8=4,
	MAX_I_6=5,
	MAX_I_5=6,
	MAX_I_4=7,
	MAX_I_3=8,
	MAX_I_2=9,
} INA226_MAX_I;

typedef enum {
	MODE_SHUTDOWN=0,
	MODE_SHUNT_TRIGGERED=1,
	MODE_BUS_TRIGGERED=2,
	MODE_SHUNT_BUS_TRIGGERED=3,
	MODE_SHUNT_CONTINUOUS=5,
	MODE_BUS_CONTINUOUS=6,
	MODE_SHUNT_BUS_CONTINUOUS=7
} INA226_MODE;

void ina226_reset(I2C_HandleTypeDef *hi2c,int ina226_addr);
ushort ina226_get_config_reg(I2C_HandleTypeDef *hi2c,int ina226_addr);
ina226_cal_t ina226_get_calibration_reg(I2C_HandleTypeDef *hi2c,int ina226_addr);
ushort ina226_get_mfg_id(I2C_HandleTypeDef *hi2c,int ina226_addr);
ushort ina226_get_die_id(I2C_HandleTypeDef *hi2c,int ina226_addr);
int ina226_calibrate(I2C_HandleTypeDef *hi2c,int ina226_addr,double i_max);
void ina226_set_max_current(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_MAX_I max_i);
void ina226_set_averaging_mode(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_AVERAGING_MODE mode);
void ina226_set_bus_vct(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_VCT ct);
void ina226_set_shunt_vct(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_VCT ct);
void ina226_set_mode(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_MODE mode);
ina226_data_t ina226_get_shunt_voltage_uv(I2C_HandleTypeDef *hi2c,int ina226_addr);
ina226_data_t ina226_get_bus_voltage_mv(I2C_HandleTypeDef *hi2c,int ina226_addr);
ina226_data_t ina226_get_power(I2C_HandleTypeDef *hi2c,int ina226_addr);
ina226_data_t ina226_get_current(I2C_HandleTypeDef *hi2c,int ina226_addr);

#endif /* INA226_H_ */
