/*
 * tests.h
 *
 *  Created on: Aug 2, 2017
 *      Author: user
 */

#ifndef TESTS_H_
#define TESTS_H_

void test_ina226_config_set(I2C_HandleTypeDef *hi2c,int ina226_addr);
void test_ina226_data(I2C_HandleTypeDef *hi2c,int ina226_addr);

#endif /* TESTS_H_ */
