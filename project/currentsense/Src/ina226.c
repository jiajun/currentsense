/*
 * ina226.c
 *
 *  Created on: Aug 2, 2017
 *      Author: user
 */
#include "stm32f0xx_hal.h"
#include "ina226.h"

#define I2C_TIMEOUT 10000
#define R_SHUNT 0.002

#define CONFIGURATION_REGISTER 		0X00
#define SHUNT_VOLTAGE_REGISTER 		0X01
#define BUS_VOLTAGE_REGISTER 		0X02
#define POWER_REGISTER 				0X03
#define CURRENT_REGISTER 			0X04
#define CALIBRATION_REGISTER 		0X05
#define MASK_REGISTER 				0X06
#define ALERT_LIMIT_REGISTER 		0X07
#define MANUFACTURER_ID_REGISTER 	0XFE
#define DIE_ID_REGISTER 			0XFF

double INA226_CURRENT_LSB = 0;
ina226_cal_t last_cal;

void ina226_reset(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	cfg = cfg | 0x8000;
	uint8_t i2c_buf[2]={(cfg>>8),(cfg&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

ina226_cal_t ina226_get_calibration_reg(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ina226_cal_t r;
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,CALIBRATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return r;}
	r.corrected =(((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
	if (last_cal.corrected==r.corrected){r.raw=last_cal.raw;}
	else {r.raw=0;}
	return r;
}

ushort ina226_get_config_reg(I2C_HandleTypeDef *hi2c,int ina226_addr){
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return 0;}
	return (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
}

ushort ina226_get_mfg_id(I2C_HandleTypeDef *hi2c,int ina226_addr){
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,MANUFACTURER_ID_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return 0;}
	return (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
}

ushort ina226_get_die_id(I2C_HandleTypeDef *hi2c,int ina226_addr){
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,DIE_ID_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return 0;}
	return (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
}

int ina226_calibrate(I2C_HandleTypeDef *hi2c,int ina226_addr,double i_max){
	//min current = 2.560078127
	//max current = 83886.08
	double min_current_lsb = i_max/32768.0;
	double current_lsb = (int)((min_current_lsb/0.00002)+1)*0.00002;
	INA226_CURRENT_LSB = current_lsb;
	int cal = 0.00512/(INA226_CURRENT_LSB*R_SHUNT);
	uint8_t i2c_buf[2]={(cal>>8),(cal&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CALIBRATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	return cal;
}

void ina226_set_max_current(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_MAX_I max_i){
	ushort cal = 0;
	switch (max_i) {
	case MAX_I_2:
		INA226_CURRENT_LSB = 0.00008;
		cal = 32000;
		break;
	case MAX_I_3:
		INA226_CURRENT_LSB = 0.0001;
		cal = 25600;
		break;
	case MAX_I_4:
		INA226_CURRENT_LSB = 0.000125;
		cal = 20480;
		break;
	case MAX_I_5:
		INA226_CURRENT_LSB = 0.00015625;
		cal = 16384;
		break;
	case MAX_I_6:
		INA226_CURRENT_LSB = 0.0002;
		cal = 12800;
		break;
	case MAX_I_8:
		INA226_CURRENT_LSB = 0.00025;
		cal = 10240;
		break;
	case MAX_I_10:
		INA226_CURRENT_LSB = 0.0003125;
		cal = 8192;
		break;
	case MAX_I_13:
		INA226_CURRENT_LSB = 0.0004;
		cal = 6400;
		break;
	case MAX_I_16:
		INA226_CURRENT_LSB = 0.0005;
		cal = 5120;
		break;
	case MAX_I_20:
	default:
		INA226_CURRENT_LSB = 0.000625;
		cal = 4096;
		break;
	}
	last_cal.raw = cal;
	//0.81 is correction factor to match external ammeter
	cal = cal * 0.8160774410;
	last_cal.corrected = cal;
	uint8_t i2c_buf[2]={(cal>>8),(cal&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CALIBRATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

void ina226_set_averaging_mode(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_AVERAGING_MODE mode){
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	mode = mode & 0x7;
	cfg = cfg & ~(0x0E00);
	cfg = cfg | (mode<<9);
	cfg = cfg & 0x7FFF; //ensure RST bit is  0
	uint8_t i2c_buf[2]={(cfg>>8),(cfg&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

void ina226_set_bus_vct(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_VCT ct){
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	ct = ct & 0x7;
	cfg = cfg & ~(0x01C0);
	cfg = cfg | (ct<<6);
	cfg = cfg & 0x7FFF; //ensure RST bit is  0
	uint8_t i2c_buf[2]={(cfg>>8),(cfg&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

void ina226_set_shunt_vct(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_VCT ct){
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	ct = ct & 0x7;
	cfg = cfg & ~(0x0038);
	cfg = cfg | (ct<<3);
	cfg = cfg & 0x7FFF; //ensure RST bit is  0
	uint8_t i2c_buf[2]={(cfg>>8),(cfg&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

void ina226_set_mode(I2C_HandleTypeDef *hi2c,int ina226_addr, INA226_MODE mode){
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	mode = mode & 0x7;
	cfg = cfg & ~(0x0007);
	cfg = cfg | (mode);
	cfg = cfg & 0x7FFF; //ensure RST bit is  0
	uint8_t i2c_buf[2]={(cfg>>8),(cfg&0xff)};
	HAL_I2C_Mem_Write(hi2c,ina226_addr,CONFIGURATION_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
}

ina226_data_t ina226_get_shunt_voltage_uv(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ina226_data_t r;
	r.raw = 0;
	r.value = 0;
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,SHUNT_VOLTAGE_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return r;}
	ushort val = (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
	r.raw = val;
	float v = 0;
	if ((val&0x8000)==0x8000){
		val = ~(val-1);
		v = 0-val;
	} else{
		v = val;
	}
	r.value = v*2.5;
	return r;
}

ina226_data_t ina226_get_bus_voltage_mv(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ina226_data_t r;
	r.raw = 0;
	r.value = 0;
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,BUS_VOLTAGE_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return r;}
	ushort val = (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
	r.raw = val;
	r.value = val*1.25;
	return r;
}

ina226_data_t ina226_get_power(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ina226_data_t r;
	r.raw = 0;
	r.value = 0;
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,POWER_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return r;}
	ushort val = (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
	r.raw = val;
	r.value = val*25*INA226_CURRENT_LSB;
	return r;
}

ina226_data_t ina226_get_current(I2C_HandleTypeDef *hi2c,int ina226_addr){
	ina226_data_t r;
	r.raw = 0;
	r.value = 0;
	uint8_t i2c_buf[2]={0,0};
	int hal = HAL_I2C_Mem_Read(hi2c,ina226_addr,CURRENT_REGISTER,1,i2c_buf,2,I2C_TIMEOUT);
	if (hal!=HAL_OK){return r;}
	ushort val = (((ushort)i2c_buf[0])<<8)|((ushort)i2c_buf[1]);
	r.raw = val;
	float v = 0;
	if ((val&0x8000)==0x8000){
		val = ~(val-1);
		v = 0-val;
	} else{
		v = val;
	}
	r.value = v*INA226_CURRENT_LSB;
	return r;
}
