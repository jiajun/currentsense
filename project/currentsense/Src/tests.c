/*
 * tests.c
 *
 *  Created on: Aug 2, 2017
 *      Author: user
 */
#include "stm32f0xx_hal.h"
#include "tests.h"
#include "ina226.h"


/*
void test_ina226_config_set(I2C_HandleTypeDef *hi2c,int ina226_addr){
	char buf[80];
	int n = 0;
	ina226_set_averaging_mode(hi2c,ina226_addr, AVERAGE_102);
	ina226_set_bus_vct(hi2c,ina226_addr, VBUSCT_US_8244);
	ina226_set_shunt_vct(hi2c,ina226_addr, VSHCT_US_8244);
	ina226_set_mode(hi2c,ina226_addr, MODE_SHUNT_BUS_CONTINUOUS);
	ushort cfg = ina226_get_config_reg(hi2c,ina226_addr);
	if (cfg==0x4FFF){
		n = snprintf(buf,80,"test_ina226_config_set - 1 : [%04X] OK\n\r",cfg);
	} else{
		n = snprintf(buf,80,"test_ina226_config_set - 1 : [%04X] NOK\n\r",cfg);
	}
	CDC_Transmit_FS(buf,n);
	ina226_reset(hi2c,ina226_addr);
	cfg = ina226_get_config_reg(hi2c,ina226_addr);
	if (cfg==0x4127){
		n = snprintf(buf,80,"test_ina226_config_set - 2 : [%04X] OK\n\r",cfg);
	} else{
		n = snprintf(buf,80,"test_ina226_config_set - 2 : [%04X] NOK\n\r",cfg);
	}
	CDC_Transmit_FS(buf,n);
	ina226_set_averaging_mode(hi2c,ina226_addr, AVERAGE_1);
	ina226_set_bus_vct(hi2c,ina226_addr, VBUSCT_US_140);
	ina226_set_shunt_vct(hi2c,ina226_addr, VSHCT_US_140);
	ina226_set_mode(hi2c,ina226_addr, MODE_SHUTDOWN);
	cfg = ina226_get_config_reg(hi2c,ina226_addr);
	if (cfg==0x4000){
		n = snprintf(buf,80,"test_ina226_config_set - 3 : [%04X] OK\n\r",cfg);
	} else{
		n = snprintf(buf,80,"test_ina226_config_set - 3 : [%04X] NOK\n\r",cfg);
	}
	CDC_Transmit_FS(buf,n);
	ina226_reset(hi2c,ina226_addr);
}
*/
void test_ina226_calibrate(I2C_HandleTypeDef *hi2c,int ina226_addr){
	char buf[80];
	char float_buf[32];
	int n=0;
	double i_max = 15;
	double R_SHUNT = 0.002;
	double min_current_lsb = i_max/32768.0;
	floatToString2(float_buf,32,min_current_lsb);
	n = snprintf(buf,32,"min_current_lsb=%s\n\r",float_buf);
	CDC_Transmit_FS(buf,n);
	HAL_Delay(500);
//	double current_lsb = 0;
//	while (current_lsb<min_current_lsb){
//		current_lsb = current_lsb + 0.00002;
//	}
	double current_lsb = (int)((min_current_lsb/0.00002)+1)*0.00002;
	floatToString2(float_buf,32,current_lsb);
	n = snprintf(buf,32,"current_lsb=%s\n\r",float_buf);
	CDC_Transmit_FS(buf,n);
	HAL_Delay(500);
	int cal = 0.00512/(current_lsb*R_SHUNT);
	n = snprintf(buf,32,"cal=%d [0x%04X]\n\r",cal,cal);
	CDC_Transmit_FS(buf,n);
	uint8_t i2c_buf[2]={(cal>>8),(cal&0xff)};
	n = snprintf(buf,32,"b0=%02X b1=%02X\n\r",i2c_buf[0],i2c_buf[1]);
	CDC_Transmit_FS(buf,n);
}

void test_ina226_data(I2C_HandleTypeDef *hi2c,int ina226_addr){
	char buf[32];
	char float_buf[32];
	int n = 0;
	CDC_Transmit_FS("DATA TEST\n\r",11);
	int cal = ina226_calibrate(hi2c,ina226_addr,15);
	n = snprintf(buf,32,"CAL=0x%04X\n\r",cal);
	CDC_Transmit_FS(buf,n);
	ina226_data_t v_shunt = ina226_get_shunt_voltage_uv(hi2c,ina226_addr);
	floatToString(float_buf,32,v_shunt.value);
	n = snprintf(buf,32,"v_shunt=%s [0x%04X]\n\r",float_buf,v_shunt.raw);
	CDC_Transmit_FS(buf,n);
	ina226_data_t v_bus = ina226_get_bus_voltage_mv(hi2c,ina226_addr);
	floatToString(float_buf,32,v_bus.value);
	n = snprintf(buf,32,"v_bus=%s [0x%04X]\n\r",float_buf,v_bus.raw);
	CDC_Transmit_FS(buf,n);
	ina226_data_t current = ina226_get_current(hi2c,ina226_addr);
	floatToString(float_buf,32,current.value);
	n = snprintf(buf,32,"current=%s [0x%04X]\n\r",float_buf,current.raw);
	CDC_Transmit_FS(buf,n);
	ina226_data_t power = ina226_get_power(hi2c,ina226_addr);
	floatToString(float_buf,32,power.value);
	n = snprintf(buf,32,"power=%s [0x%04X]\n\r",float_buf,power.raw);
	CDC_Transmit_FS(buf,n);
}
