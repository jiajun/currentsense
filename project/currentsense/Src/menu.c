/*
 * menu.c
 *
 *  Created on: Aug 3, 2017
 *      Author: user
 */
#include "stm32f0xx_hal.h"
#include "ina226.h"
#include "menu.h"

const char* MENU_PROMPT = "> ";
const char* MENU_TITLE = "%d %s: %s\n\r";
const char* MENU_MAIN_FMT = "%d. %s: %s\n\r";
const char* MENU_OPTION_FMT = "%d. %s\n\r";
const char* MENU_RESET = "Reset\n\r";
const char* MENU_MODE = "Operating Mode";
const char* MENU_VBUSCT = "Bus Voltage Conversion Time";
const char* MENU_VSHCT = "Shunt Voltage Conversion Time";
const char* MENU_AVG = "Averaging Mode";
const char* MENU_CALIBRATION = "Calibration";
const char* MENU_SAMPLE = "Sample Data";
const char* MENU_BACK = "X. Back\n\r"; //len==9
const char* MENU_INVALID = "!!! INVALID OPTION\n\r"; //len==20
const uint8_t MENU_IDX_RESET = 0;
const uint8_t MENU_IDX_MODE = 1;
const uint8_t MENU_IDX_VBUSCT = 2;
const uint8_t MENU_IDX_VSHCT = 3;
const uint8_t MENU_IDX_AVG = 4;
const uint8_t MENU_IDX_CAL = 5;
const uint8_t MENU_IDX_SAMPLE = 6;

const char* COMMA = "%s, %s";
const char* S_MODE_SHUTDOWN = "SHUTDOWN";
const char* S_MODE_SHUNT = "SHUNT VOLTAGE";
const char* S_MODE_BUS = "BUS VOLTAGE";
const char* S_MODE_SHUNT_BUS = "SHUNT AND BUS";
const char* S_MODE_TRIGGERED = "TRIGGERED";
const char* S_MODE_CONTINUOUS = "CONTINUOUS";
const char* S_CT_140 = "140 us";
const char* S_CT_204 = "204 us";
const char* S_CT_332 = "332 us";
const char* S_CT_588 = "588 us";
const char* S_CT_1100 = "1.1 ms";
const char* S_CT_2116 = "2.116 ms";
const char* S_CT_4156 = "4.156 ms";
const char* S_CT_8244 = "8.244 ms";
const char* S_MAX_I_20 = "20A 625uA/bit";
const char* S_MAX_I_16 = "16A 500uA/bit";
const char* S_MAX_I_13 = "13A 400uA/bit";
const char* S_MAX_I_10 = "10A 312uA/bit";
const char* S_MAX_I_8 = "8A 250uA/bit";
const char* S_MAX_I_6 = "6A 200uA/bit";
const char* S_MAX_I_5 = "5A 156uA/bit";
const char* S_MAX_I_4 = "4A 125uA/bit";
const char* S_MAX_I_3 = "3A 100uA/bit";
const char* S_MAX_I_2 = "2A 80uA/bit";
const char* S_VBUS = "V_Bus";
const char* S_VSHUNT = "V_Shunt";
const char* S_CURRENT = "Current";
const char* S_POWER = "Power";

typedef enum {
	MENU_PAGE_MAIN,
	MENU_PAGE_MODE,
	MENU_PAGE_VBUSCT,
	MENU_PAGE_VSHCT,
	MENU_PAGE_AVG,
	MENU_PAGE_CAL,
} menu_page_t;

menu_page_t current_page = MENU_PAGE_MAIN;
char CDC_Received_Char;
uint8_t CDC_Received;
char CDC_Received_Buf[3] = { '\0', '\n', '\r' };

void floatToString(char* c, int n, double d) {
	int i1 = (int) d;
	int i2 = (int) ((d - i1) * 1000000);
	if (i2<0){i2=i2*-1;}
	snprintf(c, n, "%d.%06d", i1, i2);
}

void usb_print(char* buf, uint16_t len) {
	if (len < 1)
		return;
	int status = 1;
	do {
		status = CDC_Transmit_FS(buf, len);
	} while (status != 0);
}

void print_menu_title(char* title) {
	char buf[80];
	int n = snprintf(buf, 80, "\n\r%s\n\r====\n\r", title);
	usb_print(buf, n);
}
void print_prompt() {
	usb_print(MENU_PROMPT, 2);
	CDC_Received = 0;
}

char read_input() {
	print_prompt();
	uint32_t tick = HAL_GetTick();
	while (CDC_Received != 1) {
		if((HAL_GetTick()-tick)>=250){
		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_8);
		tick = HAL_GetTick();
		}
	}
	char c = CDC_Received_Char;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_SET);
	CDC_Received = 0;
	CDC_Received_Buf[0] = c;
	usb_print(CDC_Received_Buf, 3);
	return c;
}

void print_reset() {
	char buf[80];
	int n = snprintf(buf, 80, "%d. %s", MENU_IDX_RESET, MENU_RESET);
	usb_print(buf, n);
}

void print_cal(ina226_cal_t cal) {
	char* fmt = "%d. %s: 0x%04X [%s]\n\r";
	char buf[80];
	int n = 0;
	switch (cal.raw) {
	case 4096:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_20);
		break;
	case 5120:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_16);
		break;
	case 6400:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_13);
		break;
	case 8192:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_10);
		break;
	case 10240:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_8);
		break;
	case 12800:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_6);
		break;
	case 16384:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_5);
		break;
	case 20480:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_4);
		break;
	case 25600:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_3);
		break;
	case 32000:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected,
				S_MAX_I_2);
		break;
	case 0:
		n = snprintf(buf, 80, fmt, MENU_IDX_CAL, MENU_CALIBRATION, cal.corrected, "");
		break;
	}
	usb_print(buf, n);
}

void print_mode(uint8_t mode) {
	char buf[80];
	char buf2[32];
	int n = 0;
	switch (mode) {
	case 0:
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE,
				S_MODE_SHUTDOWN);
		break;
	case 1:
		snprintf(buf2, 32, COMMA, S_MODE_SHUNT, S_MODE_TRIGGERED);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	case 2:
		snprintf(buf2, 32, COMMA, S_MODE_BUS, S_MODE_TRIGGERED);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	case 3:
		snprintf(buf2, 32, COMMA, S_MODE_SHUNT_BUS, S_MODE_TRIGGERED);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	case 4:
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE,
				S_MODE_SHUTDOWN);
		break;
	case 5:
		snprintf(buf2, 32, COMMA, S_MODE_SHUNT, S_MODE_CONTINUOUS);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	case 6:
		snprintf(buf2, 32, COMMA, S_MODE_BUS, S_MODE_CONTINUOUS);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	case 7:
		snprintf(buf2, 32, COMMA, S_MODE_SHUNT_BUS, S_MODE_CONTINUOUS);
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, buf2);
		break;
	default:
		n = snprintf(buf, 80, MENU_MAIN_FMT, MENU_IDX_MODE, MENU_MODE, "???");
	}
	usb_print(buf, n);
	return;
}

void print_ct(const char* t, uint8_t i, uint8_t ct) {
	char buf[80];
	int n = 0;
	switch (ct) {
	case 0:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_140);
		break;
	case 1:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_204);
		break;
	case 2:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_332);
		break;
	case 3:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_588);
		break;
	case 4:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_1100);
		break;
	case 5:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_2116);
		break;
	case 6:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_4156);
		break;
	case 7:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, S_CT_8244);
		break;
	default:
		n = snprintf(buf, 80, MENU_MAIN_FMT, i, t, "???");
	}
	usb_print(buf, n);
	return;
}

void print_vbusct(uint8_t vbus_ct) {
	print_ct(MENU_VBUSCT, MENU_IDX_VBUSCT, vbus_ct);
}

void print_vshct(uint8_t vbus_ct) {
	print_ct(MENU_VSHCT, MENU_IDX_VSHCT, vbus_ct);
}

void print_avg(uint8_t avg) {
	char buf1[5] = { '?', '?', '?', '\0' };
	char buf2[80];
	int n = 0;
	char *fmt = "%d";
	switch (avg) {
	case 0:
		n = snprintf(buf1, 5, fmt, 1);
		break;
	case 1:
		n = snprintf(buf1, 5, fmt, 4);
		break;
	case 2:
		n = snprintf(buf1, 5, fmt, 16);
		break;
	case 3:
		n = snprintf(buf1, 5, fmt, 64);
		break;
	case 4:
		n = snprintf(buf1, 5, fmt, 128);
		break;
	case 5:
		n = snprintf(buf1, 5, fmt, 256);
		break;
	case 6:
		n = snprintf(buf1, 5, fmt, 512);
		break;
	case 7:
		n = snprintf(buf1, 5, fmt, 1024);
		break;
	}
	n = snprintf(buf2, 80, MENU_MAIN_FMT, MENU_IDX_AVG, MENU_AVG, buf1);
	usb_print(buf2, n);
	return;
}

void print_sample(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char* buf[80];
	char* buf_float[32];
	int n = 0;
	char* fmt = "%s=%s ";
	ina226_data_t vshunt = ina226_get_shunt_voltage_uv(hi2c, ina226_addr);
	ina226_data_t vbus = ina226_get_bus_voltage_mv(hi2c, ina226_addr);
	ina226_data_t power = ina226_get_power(hi2c, ina226_addr);
	ina226_data_t current = ina226_get_current(hi2c, ina226_addr);
	usb_print("\n\r", 2);
	floatToString(buf_float,32, vshunt.value);
	n = snprintf(buf, 80, fmt, S_VSHUNT, buf_float);
	usb_print(buf, n);
	floatToString(buf_float,32, vbus.value);
	n = snprintf(buf, 80, fmt, S_VBUS, buf_float);
	usb_print(buf, n);
	floatToString(buf_float,32, current.value);
	n = snprintf(buf, 80, fmt, S_CURRENT, buf_float);
	usb_print(buf, n);
	floatToString(buf_float,32, power.value);
	n = snprintf(buf, 80, fmt, S_POWER, buf_float);
	usb_print(buf, n);
	usb_print("\n\r", 2);
}

void print_continuous(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char* buf[90];
	int n = 0;
	int status = 0;
	char* fmt = "[%d,%d.%06d,%d.%06d,%d.%06d,%d.%06d]\n\r";
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,GPIO_PIN_RESET);
	while(1){
	ina226_data_t vshunt = ina226_get_shunt_voltage_uv(hi2c, ina226_addr);
	ina226_data_t vbus = ina226_get_bus_voltage_mv(hi2c, ina226_addr);
	ina226_data_t power = ina226_get_power(hi2c, ina226_addr);
	ina226_data_t current = ina226_get_current(hi2c, ina226_addr);
	int vs1 = (int) vshunt.value;
	int vs2 = (int) ((vshunt.value - vs1) * 1000000);
	if (vs2<0)vs2=-vs2;
	int vb1 = (int) vbus.value;
	int vb2 = (int) ((vbus.value - vb1) * 1000000);
	if (vb2<0)vb2=-vb2;
	int p1 = (int) power.value;
	int p2 = (int) ((power.value - p1) * 1000000);
	if (p2<0)p2=-p2;
	int c1 = (int) current.value;
	int c2 = (int) ((current.value - c1) * 1000000);
	if (c2<0)c2=-c2;
	uint32_t tick = HAL_GetTick();
	n=snprintf(buf,90,fmt,tick,vs1,vs2,vb1,vb2,p1,p2,c1,c2);
	if (n<1){continue;}
		do {
			status = CDC_Transmit_FS(buf, n);
		} while (status != 0);
	//HAL_Delay(100);
	}
}

void menu_main(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char buf[80];
	int n = 0;
	ushort cfg = ina226_get_config_reg(hi2c, ina226_addr);
	uint8_t mode = cfg & 0x7;
	uint8_t vshct = (cfg >> 3) & 0x7;
	uint8_t vbusct = (cfg >> 6) & 0x7;
	uint8_t avg = (cfg >> 9) & 0x7;
	ina226_cal_t cal = ina226_get_calibration_reg(hi2c, ina226_addr);
	print_menu_title("Menu");
	print_reset();
	print_mode(mode);
	print_vbusct(vbusct);
	print_vshct(vshct);
	print_avg(avg);
	print_cal(cal);
	n= snprintf(buf, 80, "%d. %s\n\r", MENU_IDX_SAMPLE, MENU_SAMPLE);
	usb_print(buf, n);
	usb_print("G. GO\n\r", 7);
	char c = read_input();
	switch (c) {
	case '0':
		ina226_reset(hi2c, ina226_addr);
		break;
	case '1':
		current_page = MENU_PAGE_MODE;
		break;
	case '2':
		current_page = MENU_PAGE_VBUSCT;
		break;
	case '3':
		current_page = MENU_PAGE_VSHCT;
		break;
	case '4':
		current_page = MENU_PAGE_AVG;
		break;
	case '5':
		current_page = MENU_PAGE_CAL;
		break;
	case '6':
		print_sample(hi2c,ina226_addr);
			current_page = MENU_PAGE_MAIN;
			break;
	case 'G':
	case 'g':
		print_continuous(hi2c,ina226_addr);
	default:
		usb_print(MENU_INVALID, 20);
		current_page = MENU_PAGE_MAIN;
	}
	return;
}

void menu_mode(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char buf[80];
	char buf2[80];
	int n = 0;
	print_menu_title(MENU_MODE);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 0, S_MODE_SHUTDOWN);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_SHUNT, S_MODE_TRIGGERED);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 1, buf2);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_BUS, S_MODE_TRIGGERED);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 2, buf2);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_SHUNT_BUS, S_MODE_TRIGGERED);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 3, buf2);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_SHUNT, S_MODE_CONTINUOUS);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 4, buf2);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_BUS, S_MODE_CONTINUOUS);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 5, buf2);
	usb_print(buf, n);
	snprintf(buf2, 32, COMMA, S_MODE_SHUNT_BUS, S_MODE_CONTINUOUS);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 6, buf2);
	usb_print(buf, n);
	usb_print(MENU_BACK, 9);
	char c = read_input();
	current_page = MENU_PAGE_MAIN;
	switch (c) {
	case '0':
		ina226_set_mode(hi2c, ina226_addr, MODE_SHUTDOWN);
		break;
	case '1':
		ina226_set_mode(hi2c, ina226_addr, MODE_SHUNT_TRIGGERED);
		break;
	case '2':
		ina226_set_mode(hi2c, ina226_addr, MODE_BUS_TRIGGERED);
		break;
	case '3':
		ina226_set_mode(hi2c, ina226_addr, MODE_SHUNT_BUS_TRIGGERED);
		break;
	case '4':
		ina226_set_mode(hi2c, ina226_addr, MODE_SHUNT_CONTINUOUS);
		break;
	case '5':
		ina226_set_mode(hi2c, ina226_addr, MODE_BUS_CONTINUOUS);
		break;
	case '6':
		ina226_set_mode(hi2c, ina226_addr, MODE_SHUNT_BUS_CONTINUOUS);
		break;
	case 'x':
	case 'X':
		current_page = MENU_PAGE_MAIN;
		break;
	default:
		usb_print(MENU_INVALID, 20);
		current_page = MENU_PAGE_MODE;
	}
}

void menu_ct(I2C_HandleTypeDef *hi2c, int ina226_addr, char* menu_title,
		menu_page_t page, void (*f)(I2C_HandleTypeDef*, int, INA226_VCT)) {
	char buf[80];
	int n = 0;
	print_menu_title(menu_title);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 0, S_CT_140);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 1, S_CT_204);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 2, S_CT_332);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 3, S_CT_588);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 4, S_CT_1100);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 5, S_CT_2116);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 6, S_CT_4156);
	usb_print(buf, n);
	n = snprintf(buf, 80, MENU_OPTION_FMT, 7, S_CT_8244);
	usb_print(buf, n);
	usb_print(MENU_BACK, 9);
	char c = read_input();
	current_page = MENU_PAGE_MAIN;
	switch (c) {
	case '0':
		f(hi2c, ina226_addr, VCT_US_140);
		break;
	case '1':
		f(hi2c, ina226_addr, VCT_US_204);
		break;
	case '2':
		f(hi2c, ina226_addr, VCT_US_332);
		break;
	case '3':
		f(hi2c, ina226_addr, VCT_US_588);
		break;
	case '4':
		f(hi2c, ina226_addr, VCT_US_1100);
		break;
	case '5':
		f(hi2c, ina226_addr, VCT_US_2116);
		break;
	case '6':
		f(hi2c, ina226_addr, VCT_US_4156);
		break;
	case '7':
		f(hi2c, ina226_addr, VCT_US_8244);
		break;
	case 'x':
	case 'X':
		current_page = MENU_PAGE_MAIN;
		break;
	default:
		usb_print(MENU_INVALID, 20);
		current_page = page;
	}
}

void menu_vbusct(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	menu_ct(hi2c, ina226_addr, MENU_VBUSCT, MENU_PAGE_VBUSCT,
			ina226_set_bus_vct);
}

void menu_vshct(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	menu_ct(hi2c, ina226_addr, MENU_VSHCT, MENU_PAGE_VSHCT,
			ina226_set_shunt_vct);
}

void menu_avg(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char buf[80];
	char *fmt = "%d. %d\n\r";
	int n = 0;
	print_menu_title(MENU_AVG);
	n = snprintf(buf, 80, fmt, 0, 1);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 1, 4);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 2, 16);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 3, 64);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 4, 128);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 5, 256);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 6, 512);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 7, 1024);
	usb_print(buf, n);
	usb_print(MENU_BACK, 9);
	char c = read_input();
	current_page = MENU_PAGE_MAIN;
	switch (c) {
	case '0':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_1);
		break;
	case '1':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_4);
		break;
	case '2':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_16);
		break;
	case '3':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_64);
		break;
	case '4':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_128);
		break;
	case '5':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_256);
		break;
	case '6':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_512);
		break;
	case '7':
		ina226_set_averaging_mode(hi2c, ina226_addr, AVERAGE_1024);
		break;
	case 'x':
	case 'X':
		current_page = MENU_PAGE_MAIN;
		break;
	default:
		usb_print(MENU_INVALID, 20);
		current_page = MENU_PAGE_AVG;
	}
}

void menu_calibration(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	char buf[80];
	char *fmt = "%d. %s\n\r";
	int n = 0;
	print_menu_title(MENU_CALIBRATION);
	const char* S_MAX_I_20 = "20A 625uA/bit";
	const char* S_MAX_I_16 = "16A 500uA/bit";
	const char* S_MAX_I_13 = "13A 400uA/bit";
	const char* S_MAX_I_10 = "10A 312uA/bit";
	const char* S_MAX_I_8 = " 8A 250uA/bit";
	const char* S_MAX_I_6 = " 6A 200uA/bit";
	const char* S_MAX_I_5 = " 5A 156uA/bit";
	const char* S_MAX_I_4 = " 4A 125uA/bit";
	const char* S_MAX_I_3 = " 3A 100uA/bit";
	const char* S_MAX_I_2 = " 2A 80uA/bit";

	n = snprintf(buf, 80, fmt, 0, S_MAX_I_20);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 1, S_MAX_I_16);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 2, S_MAX_I_13);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 3, S_MAX_I_10);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 4, S_MAX_I_8);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 5, S_MAX_I_6);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 6, S_MAX_I_5);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 7, S_MAX_I_4);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 8, S_MAX_I_3);
	usb_print(buf, n);
	n = snprintf(buf, 80, fmt, 9, S_MAX_I_2);
	usb_print(buf, n);
	usb_print(MENU_BACK, 9);
	char c = read_input();
	current_page = MENU_PAGE_MAIN;
	switch (c) {
	case '0':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_20);
		break;
	case '1':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_16);
		break;
	case '2':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_13);
		break;
	case '3':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_10);
		break;
	case '4':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_8);
		break;
	case '5':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_6);
		break;
	case '6':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_5);
		break;
	case '7':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_4);
		break;
	case '8':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_3);
		break;
	case '9':
		ina226_set_max_current(hi2c, ina226_addr, MAX_I_2);
		break;
	case 'x':
	case 'X':
		current_page = MENU_PAGE_MAIN;
		break;
	default:
		usb_print(MENU_INVALID, 20);
		current_page = MENU_PAGE_CAL;
	}
}

void menu(I2C_HandleTypeDef *hi2c, int ina226_addr) {
	ina226_reset(hi2c, ina226_addr);
	ina226_set_max_current(hi2c, ina226_addr, MAX_I_20);
	while (1) {
		switch (current_page) {
		case MENU_PAGE_MAIN:
			menu_main(hi2c, ina226_addr);
			break;
		case MENU_PAGE_MODE:
			menu_mode(hi2c, ina226_addr);
			break;
		case MENU_PAGE_VBUSCT:
			menu_vbusct(hi2c, ina226_addr);
			break;
		case MENU_PAGE_VSHCT:
			menu_vshct(hi2c, ina226_addr);
			break;
		case MENU_PAGE_AVG:
			menu_avg(hi2c, ina226_addr);
			break;
		case MENU_PAGE_CAL:
			menu_calibration(hi2c, ina226_addr);
			break;
		default:
			menu_main(hi2c, ina226_addr);
			break;
		}
	}
}
